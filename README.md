[![CircleCI](https://circleci.com/bb/kimis/spring5-mvc-rest.svg?style=svg)](https://circleci.com/bb/kimis/spring5-mvc-res)
# Spring Framework 5 MVC Rest Application

[![codecov](https://codecov.io/bb/kimis/spring5-mvc-rest/branch/master/graph/badge.svg)](https://codecov.io/bb/kimis/spring5-mvc-rest)

This repository is for an example application built in my Spring Framework 5 - Beginner to Guru

You can learn about my Spring Framework 5 Online course [here.](http://courses.springframework.guru/p/spring-framework-5-begginer-to-guru/?product_id=363173)